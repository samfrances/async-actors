from asyncio.queues import Queue
import asyncio


async def _run_actor(actor, receive):

    while True:
        msg = await receive()
        message_type = msg["type"]
        method = getattr(actor, message_type, None)
        if method:
            args = msg["args"]
            kwargs = msg["kwargs"]
            await method(*args, **kwargs)


def spawn(actor):

    inbox = Queue()

    async def put(item):
        await inbox.put(item)

    async def receive():
        return await inbox.get()

    ref = ActorRef(put)
    actor.ref = ref

    asyncio.ensure_future(_run_actor(actor, receive))
    return ref


class ActorRef:

    def __init__(self, put):
        self._put = put

    async def send(self, message_type, *args, **kwargs):
        await self._put({
            "type": message_type,
            "args": args,
            "kwargs": kwargs
        })
