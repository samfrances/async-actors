import asyncio
from actors import spawn


class Printer:

    async def print(self, msg_str):
        print(msg_str)

    async def greet(self, name):
        print(f"Hello {name}")

    async def print_sum(self, adder, a, b):
        await adder.send("add", self.ref, "print", a, b)


class Adder:

    async def add(self, reply, reply_message_type, a, b):
        sum = a + b
        await reply.send(reply_message_type, sum)


async def main():

    printer = spawn(Printer())
    adder = spawn(Adder())

    await printer.send("greet", "Bob")
    await printer.send("print", "goodbye")
    await printer.send("print_sum", adder, 5, 6)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()
